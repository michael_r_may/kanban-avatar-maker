//
//  Created by Developer on 2014/03/23.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KAMColorMap : NSObject

+(NSInteger)numberOfColors;

+(UIColor*)colorForIndex:(NSUInteger)index;

@end
