//
//  Created by Developer on 2014/03/23.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import "KAMColorMap.h"

@implementation KAMColorMap

static const NSArray *colorMap;

NS_INLINE CGFloat colorComponentValueFromValue(CGFloat value)
{
    return (value / 255.0f);
}

+(void)initialize
{
    UIColor *red = [UIColor colorWithRed:colorComponentValueFromValue(255.0f)
                                   green:colorComponentValueFromValue(0.0f)
                                    blue:colorComponentValueFromValue(0.0f)
                                   alpha:1.0f];
    
    UIColor *orange = [UIColor colorWithRed:colorComponentValueFromValue(255.0f)
                                   green:colorComponentValueFromValue(125.0f)
                                    blue:colorComponentValueFromValue(0.0f)
                                   alpha:1.0f];
    
    UIColor *yellow = [UIColor colorWithRed:colorComponentValueFromValue(255.0f)
                                    green:colorComponentValueFromValue(255.0f)
                                     blue:colorComponentValueFromValue(0.0f)
                                    alpha:1.0f];
    
    UIColor *springGreen = [UIColor colorWithRed:colorComponentValueFromValue(125.0f)
                                    green:colorComponentValueFromValue(255.0f)
                                     blue:colorComponentValueFromValue(0.0f)
                                    alpha:1.0f];
    
    UIColor *green = [UIColor colorWithRed:colorComponentValueFromValue(0.0f)
                                     green:colorComponentValueFromValue(255.0f)
                                      blue:colorComponentValueFromValue(0.0f)
                                     alpha:1.0f];
    
    UIColor *turqoise = [UIColor colorWithRed:colorComponentValueFromValue(0.0f)
                                        green:colorComponentValueFromValue(255.0f)
                                         blue:colorComponentValueFromValue(125.0f)
                                        alpha:1.0f];
    
    UIColor *cyan = [UIColor colorWithRed:colorComponentValueFromValue(0.0f)
                                           green:colorComponentValueFromValue(255.0f)
                                            blue:colorComponentValueFromValue(255.0f)
                                           alpha:1.0f];
    
    UIColor *ocean = [UIColor colorWithRed:colorComponentValueFromValue(0.0f)
                                        green:colorComponentValueFromValue(125.0f)
                                         blue:colorComponentValueFromValue(255.0f)
                                        alpha:1.0f];
    
    UIColor *blue = [UIColor colorWithRed:colorComponentValueFromValue(0.0f)
                                    green:colorComponentValueFromValue(0.0f)
                                     blue:colorComponentValueFromValue(255.0f)
                                    alpha:1.0f];
    
    UIColor *violet = [UIColor colorWithRed:colorComponentValueFromValue(125.0f)
                                      green:colorComponentValueFromValue(0.0f)
                                       blue:colorComponentValueFromValue(255.0f)
                                      alpha:1.0f];
    
    UIColor *magenta = [UIColor colorWithRed:colorComponentValueFromValue(255.0f)
                                      green:colorComponentValueFromValue(0.0f)
                                       blue:colorComponentValueFromValue(255.0f)
                                      alpha:1.0f];
    
    UIColor *raspberry = [UIColor colorWithRed:colorComponentValueFromValue(255.0f)
                                      green:colorComponentValueFromValue(0.0f)
                                       blue:colorComponentValueFromValue(125.0f)
                                      alpha:1.0f];
    
    UIColor *black = [UIColor colorWithRed:colorComponentValueFromValue(0.0f)
                                     green:colorComponentValueFromValue(0.0f)
                                      blue:colorComponentValueFromValue(0.0f)
                                     alpha:1.0f];    
    
    UIColor *darkGray = [UIColor colorWithRed:colorComponentValueFromValue(192.0f)
                                         green:colorComponentValueFromValue(192.0f)
                                          blue:colorComponentValueFromValue(192.0f)
                                         alpha:1.0f];
    
    UIColor *lightGray = [UIColor colorWithRed:colorComponentValueFromValue(125.0f)
                                         green:colorComponentValueFromValue(125.0f)
                                          blue:colorComponentValueFromValue(125.0f)
                                         alpha:1.0f];
    
    UIColor *white = [UIColor colorWithRed:colorComponentValueFromValue(255.0f)
                                     green:colorComponentValueFromValue(255.0f)
                                      blue:colorComponentValueFromValue(255.0f)
                                     alpha:1.0f];

    colorMap = @[red, orange, yellow,
                 green, springGreen, turqoise,
                 cyan, ocean, blue,
                 violet, magenta, raspberry,
                 lightGray, darkGray, white, black];
}

#pragma mark - 

+(NSInteger)numberOfColors
{
    return [colorMap count];
}

+(UIColor*)colorForIndex:(NSUInteger)index
{
    if(index > ([colorMap count] - 1)) return nil;
    
    return [colorMap objectAtIndex:index];
}

@end
