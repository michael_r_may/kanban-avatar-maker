//
//  Created by Developer on 2014/03/22.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import "KAMColourPickerCollectionViewCell.h"

@implementation KAMColourPickerCollectionViewCell

-(void)adjustAlphaForState:(BOOL)state
{
    CGFloat alpha = 1.0f;
    
    if(state) {
        alpha = 0.5f;
    }
    
    [self setAlpha:alpha];
}

-(void)setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];

    [self adjustAlphaForState:highlighted];
}

-(void)setSelected:(BOOL)selected
{
    [super setSelected:selected];

    [self adjustAlphaForState:selected];
}

@end
