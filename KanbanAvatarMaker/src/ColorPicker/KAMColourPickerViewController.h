//
//  Created by Developer on 2014/03/22.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MMDismissBlockProtocol.h"

@interface KAMColourPickerViewController : UICollectionViewController <MMDismissBlockProtocol>

+(instancetype)colourPicker;

@end
