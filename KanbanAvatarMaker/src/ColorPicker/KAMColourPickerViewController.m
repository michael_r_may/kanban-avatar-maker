//
//  Created by Developer on 2014/03/22.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import "MMCGFloat64Helper.h"

#import "KAMColourPickerViewController.h"

#import "KAMColourPickerCollectionViewCell.h"

#import "KAMColorMap.h"

@interface KAMColourPickerViewController () <UICollectionViewDataSource, UICollectionViewDelegate>
@end

@implementation KAMColourPickerViewController

@synthesize dismissBlock;

#pragma mark - UICollectionViewDataSource

static NSString * const KAMColourPickerViewControllerCellID = @"KAMColourPickerViewControllerCellID";

-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [[self collectionView] dequeueReusableCellWithReuseIdentifier:KAMColourPickerViewControllerCellID
                                                                                  forIndexPath:indexPath];

    NSInteger index = [indexPath row];
    UIColor *color = [KAMColorMap colorForIndex:index];
    [cell setBackgroundColor:color];
    
    return cell;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [KAMColorMap numberOfColors];
}

#pragma mark -

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    UIColor *color = [cell backgroundColor];
    
    if(self.dismissBlock) {
        self.dismissBlock(color);
    }
}

#pragma mark - 

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    [[self collectionView] registerClass:[KAMColourPickerCollectionViewCell class]
              forCellWithReuseIdentifier:KAMColourPickerViewControllerCellID];
}

#pragma mark - 

-(instancetype)init
{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    CGRect fullScreenFrame = [[UIScreen mainScreen] bounds];
    
    CGFloat tileWidth = floor64Helper(CGRectGetWidth(fullScreenFrame) / 4.0f);
    CGFloat tileHeight = floor64Helper(CGRectGetHeight(fullScreenFrame) / 4.0f);
    
    [flowLayout setItemSize:CGSizeMake(tileWidth, tileHeight)];
    [flowLayout setMinimumInteritemSpacing:0.0f];
    [flowLayout setMinimumLineSpacing:0.0f];
    
    self = [super initWithCollectionViewLayout:flowLayout];
        
    return self;
}

#pragma mark -

+(instancetype)colourPicker
{
    return [[self alloc] init];
}

@end
