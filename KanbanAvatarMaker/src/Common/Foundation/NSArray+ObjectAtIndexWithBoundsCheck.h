//
//  Created by Developer on 2014/05/04.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (ObjectAtIndexWithBoundsCheck)

-(id)objectAtIndexWithBoundsCheck:(NSUInteger)index;

@end
