//
//  Created by Developer on 2014/05/04.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import "NSArray+ObjectAtIndexWithBoundsCheck.h"

@implementation NSArray (ObjectAtIndexWithBoundsCheck)

-(id)objectAtIndexWithBoundsCheck:(NSUInteger)index
{
    if(index >= [self count]) return nil;
    
    return [self objectAtIndex:index];
}

@end
