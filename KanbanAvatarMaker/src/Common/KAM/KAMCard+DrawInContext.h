//
//  Created by Developer on 2014/05/09.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import "KAMCard.h"

@interface KAMCard (DrawInContext)

-(void)drawCardInContext:(CGContextRef)context rect:(CGRect)rect;

@end
