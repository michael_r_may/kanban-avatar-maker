//
//  Created by Developer on 2014/05/09.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import <tgmath.h>

#import "MMCGFloat64Helper.h"

#import "KAMCard+DrawInContext.h"

#import "KAMCard+Extensions.h"
#import "KAMColor+Extensions.h"

#import "MMRoundedRectView.h"

@implementation KAMCard (DrawInContext)

// calculate these at runtime
static const CGFloat containerSizeWidth = 280.0f;

static const CGFloat imageSize = 240.0f;
static const CGFloat imageSizeAsRatioOfTotal = (imageSize / containerSizeWidth);

static const CGFloat imageToFirstLineLinePadding = 5.0f;
static const CGFloat firstLineSecondLinePadding = 20.0f + imageToFirstLineLinePadding;

// TODO: we probably need to take into account the orignal screen aspect
// ratio of the card when working out the size on the printed version so that
// they more accurately approximate each other

#pragma mark -

-(void)drawCardInContext:(CGContextRef)context
                    rect:(CGRect)rect
{
    [[self class] drawCard:self
                 inContext:context
                      rect:rect];
}

+(void)drawCard:(KAMCard*)card
      inContext:(CGContextRef)context
           rect:(CGRect)rect
{
    static const CGFloat KAMCardCuttingRoomBorder = 5.0f;
    
    CGRect rectWithCuttingRoom = CGRectInset(rect, KAMCardCuttingRoomBorder, KAMCardCuttingRoomBorder);
    
    CGFloat scale = CGRectGetWidth(rectWithCuttingRoom) / containerSizeWidth;
    
    CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
    CGContextFillRect(context, rect);
    
    [self drawBorderForCard:card inContext:context rect:rectWithCuttingRoom scale:scale];
    [self drawAvatarForCard:card inContext:context rect:rectWithCuttingRoom scale:scale];
    [self drawFirstLineForCard:card inContext:context rect:rectWithCuttingRoom scale:scale];
    [self drawSecondLineForCard:card inContext:context rect:rectWithCuttingRoom scale:scale];
}

+(void)drawBorderForCard:(KAMCard*)card
               inContext:(CGContextRef)context
                    rect:(CGRect)rect
                   scale:(CGFloat)scale
{
    UIRectCorner corners = [card corners];
    CGFloat cornerRadius = ceil64Helper([card cornerRadius] * scale);
    CGSize cornerRadii = CGSizeMake(cornerRadius, cornerRadius);
    CGFloat strokeWidth = ceil64Helper([card strokeWidth] * scale);
    UIColor *strokeColor = [[card strokeColor] color];
    
    [MMRoundedRectView drawRoundedRect:rect
                           withCorners:corners
                                 radii:cornerRadii
                           strokeWidth:strokeWidth
                           strokeColor:strokeColor.CGColor
                             inContext:context];
}

+(void)drawAvatarForCard:(KAMCard*)card
               inContext:(CGContextRef)context
                    rect:(CGRect)rect
                   scale:(CGFloat)scale
{
    CGRect avatarRect = [self rectForAvatarInRect:rect];
    
    UIImage *avatar = [card avatarImage];
    [avatar drawInRect:avatarRect blendMode:kCGBlendModeNormal alpha:1.0f];
}

+(void)drawFirstLineForCard:(KAMCard*)card
                  inContext:(CGContextRef)context
                       rect:(CGRect)rect
                      scale:(CGFloat)scale
{
    NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
    
    // DRY
    UIFont *font = [self firstLineFontForCard:card scale:scale];
    if(font) {
        [attributes setObject:font forKey:NSFontAttributeName];
    }
    
    UIColor *textColor = [[card firstLineTextColor] color];
    if(textColor) {
        [attributes setObject:textColor forKey:NSForegroundColorAttributeName];
    }
    
    CGRect avatarRect = [self rectForAvatarInRect:rect];
    CGRect firstLineRect = avatarRect;
    firstLineRect.origin.y += round(imageToFirstLineLinePadding * scale);
    firstLineRect.origin.y += CGRectGetHeight(avatarRect);
    firstLineRect.size.height = [font lineHeight];
    
    NSString *firstLine = [card name];
    
    [firstLine drawInRect:firstLineRect withAttributes:attributes];
}

+(void)drawSecondLineForCard:(KAMCard*)card
                   inContext:(CGContextRef)context
                        rect:(CGRect)rect
                       scale:(CGFloat)scale
{
    NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
    
    UIFont *firstLineFont = [self firstLineFontForCard:card scale:scale];
    UIFont *font = [self secondLineFontForCard:card scale:scale];
    if(font) {
        [attributes setObject:font forKey:NSFontAttributeName];
    }
    
    UIColor *textColor = [[card secondLineTextColor] color];
    if(textColor) {
        [attributes setObject:textColor forKey:NSForegroundColorAttributeName];
    }
    
    // hacky
    CGRect avatarRect = [self rectForAvatarInRect:rect];
    CGRect secondLineRect = avatarRect;
    secondLineRect.origin.y += CGRectGetHeight(avatarRect);
    secondLineRect.origin.y += firstLineFont.lineHeight;
    secondLineRect.origin.y += round(firstLineSecondLinePadding * scale);
    secondLineRect.size.height = [font lineHeight];
    
    NSString *secondLine = [card title];
    [secondLine drawInRect:secondLineRect withAttributes:attributes];
}

#pragma mark - utility

+(CGRect)rectForAvatarInRect:(CGRect)rect
{
    CGFloat avatarSizeLength = round64Helper(CGRectGetWidth(rect) * imageSizeAsRatioOfTotal);
    
    CGSize avatarSize = CGSizeMake(avatarSizeLength,
                                   avatarSizeLength);
    
    CGFloat avatarOriginX = round64Helper((CGRectGetWidth(rect) - avatarSizeLength) / 2.0f);
    CGFloat avatarOriginY = avatarOriginX;
    
    CGPoint avatarOrigin = CGPointMake(rect.origin.x + avatarOriginX,
                                       rect.origin.y + avatarOriginY);
    
    CGRect avatarRect = CGRectMake(avatarOrigin.x,
                                   avatarOrigin.y,
                                   avatarSize.width,
                                   avatarSize.height);
    
    return avatarRect;
}

+(UIFont*)font:(UIFont*)font
     withScale:(CGFloat)scale
{
    CGFloat fontSize = [font pointSize];
    fontSize = round64Helper(fontSize * scale);
    
    font = [font fontWithSize:fontSize];
    
    return font;
}

+(UIFont*)secondLineFontForCard:(KAMCard*)card
                          scale:(CGFloat)scale
{
    UIFont *font = [card secondLineFont];
    return [self font:font withScale:scale];
}

+(UIFont*)firstLineFontForCard:(KAMCard*)card
                         scale:(CGFloat)scale
{
    UIFont *font = [card firstLineFont];
    return [self font:font withScale:scale];
}

@end
