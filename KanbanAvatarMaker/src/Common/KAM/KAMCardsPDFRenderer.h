//
//  Created by Developer on 2014/05/09.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KAMCardsPDFRenderer : NSObject

-(NSData*)render;

+(instancetype)rendererForCards:(NSArray*)cards;

@end
