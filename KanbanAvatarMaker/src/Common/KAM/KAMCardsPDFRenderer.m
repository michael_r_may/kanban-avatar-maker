//
//  Created by Developer on 2014/05/09.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import "KAMCardsPDFRenderer.h"

#import "KAMCardsRenderer.h"

@interface KAMCardsPDFRenderer ()
@property (nonatomic, strong, readonly) KAMCardsRenderer *cardsRenderer;
@end

@implementation KAMCardsPDFRenderer

-(NSData*)render
{
    NSMutableData *pdfData = [[NSMutableData alloc] init];
    
    NSDictionary *documentInfo = @{(NSString*)kCGPDFContextAuthor : @"Kanban Avatar Maker"};
    CGRect defaultSizeLetter = {{0.0f,0.0f},{612.0f,792.0f}};
    
    UIGraphicsBeginPDFContextToData(pdfData, defaultSizeLetter, documentInfo);
    
    NSInteger numberOfPagesNeeded = [[self cardsRenderer] numberOfPages];
    
    for(NSInteger pageIndex = 0; pageIndex < numberOfPagesNeeded; pageIndex++) {
        UIGraphicsBeginPDFPage();
        
        [[self cardsRenderer] drawContentForPageAtIndex:pageIndex inRect:defaultSizeLetter];
    }

    UIGraphicsEndPDFContext();
    
    return pdfData;
}

#pragma mark -

-(instancetype)initWithCards:(NSArray*)cards
{
    if(cards == nil) return nil;
    if([cards count] == 0) return nil;
    
    self = [super init];
    
    if(self) {
        _cardsRenderer = [KAMCardsRenderer rendererForCards:cards];
    }
    
    return self;
}

#pragma mark -

+(instancetype)rendererForCards:(NSArray*)cards
{
    return [[self alloc] initWithCards:cards];
}

@end
