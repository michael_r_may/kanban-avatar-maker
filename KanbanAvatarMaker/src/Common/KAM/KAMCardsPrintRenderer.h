//
//  Created by Developer on 2014/04/30.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KAMCardsPrintRenderer : UIPrintPageRenderer

+(instancetype)rendererForCards:(NSArray*)cards;

@end
