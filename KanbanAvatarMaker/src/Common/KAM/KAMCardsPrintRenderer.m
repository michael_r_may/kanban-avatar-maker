//
//  Created by Developer on 2014/04/30.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import "KAMCardsPrintRenderer.h"
#import "KAMCardsRenderer.h"

@interface KAMCardsPrintRenderer ()
@property (nonatomic, strong, readonly) KAMCardsRenderer *cardsRenderer;
@end

@implementation KAMCardsPrintRenderer

- (NSInteger)numberOfPages
{
    return [[self cardsRenderer] numberOfPages];
}

- (void)drawContentForPageAtIndex:(NSInteger)pageIndex inRect:(CGRect)contentRect
{
    [[self cardsRenderer] drawContentForPageAtIndex:pageIndex inRect:contentRect];
}

#pragma mark - 

-(instancetype)initWithCards:(NSArray*)cards
{
    if(cards == nil) return nil;
    if([cards count] == 0) return nil;

    self = [super init];
    
    if(self) {
        _cardsRenderer = [KAMCardsRenderer rendererForCards:cards];
    }
    
    return self;
}

#pragma mark -

+(instancetype)rendererForCards:(NSArray*)cards
{
    return [[self alloc] initWithCards:cards];
}

@end
