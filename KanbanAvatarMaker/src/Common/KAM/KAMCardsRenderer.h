//
//  Created by Developer on 2014/04/30.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KAMCardsRenderer : NSObject

- (void)drawContentForPageAtIndex:(NSInteger)pageIndex inRect:(CGRect)contentRect;

- (NSInteger)numberOfPages;

+(instancetype)rendererForCards:(NSArray*)cards;

@end
