//
//  Created by Developer on 2014/04/30.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import "KAMCardsRenderer.h"

#import "KAMCard+DrawInContext.h"
#import "KAMPageRowColumnToArrayIndex.h"

#import "NSArray+ObjectAtIndexWithBoundsCheck.h"

#import "MMCGFloat64Helper.h"

@interface KAMCardsRenderer ()
@property (nonatomic, copy, readonly) NSArray *cards;   // of KAMCard
@end

@implementation KAMCardsRenderer

#pragma mark -

static const NSInteger KAMCardsPrintRendererNumberOfRowsPerPage = 2;
static const NSInteger KAMCardsPrintRendererNumberOfColsPerPage = 2;

- (NSInteger)numberOfPages
{
    NSInteger numberOfCards = [[self cards] count];
    CGFloat numberOfCardsPerPage = (KAMCardsPrintRendererNumberOfRowsPerPage * KAMCardsPrintRendererNumberOfColsPerPage);
    
    CGFloat numberOfPages = ceil64Helper((CGFloat)numberOfCards / numberOfCardsPerPage);
    
    return (NSInteger)numberOfPages;
}

- (void)drawContentForPageAtIndex:(NSInteger)pageIndex inRect:(CGRect)contentRect
{
    CGContextRef context = UIGraphicsGetCurrentContext();

    UIRectClip(contentRect);
    
    NSInteger colWidth = (NSInteger)round64Helper(contentRect.size.width / KAMCardsPrintRendererNumberOfColsPerPage);
    NSInteger rowHeight = (NSInteger)round64Helper(contentRect.size.height / KAMCardsPrintRendererNumberOfRowsPerPage);
    
    for(NSUInteger rowIndex = 0; rowIndex < KAMCardsPrintRendererNumberOfRowsPerPage; rowIndex++) {
        for(NSUInteger columnIndex = 0; columnIndex < KAMCardsPrintRendererNumberOfColsPerPage; columnIndex++) {
            NSInteger cardIndex = KAMPageRowColumnToArrayIndex(pageIndex,
                                                               rowIndex,
                                                               columnIndex,
                                                               KAMCardsPrintRendererNumberOfRowsPerPage,
                                                               KAMCardsPrintRendererNumberOfColsPerPage);
            
            KAMCard *cardAtIndex = [[self cards] objectAtIndexWithBoundsCheck:cardIndex];
            
            if(cardAtIndex == nil) break;
            
            CGFloat startX = round64Helper(contentRect.origin.x + columnIndex * colWidth);
            CGFloat startY = round64Helper(contentRect.origin.y + rowIndex * rowHeight);
            CGRect cardRect = CGRectMake(startX, startY, colWidth, rowHeight);

            [cardAtIndex drawCardInContext:context rect:cardRect];
        }
    }
}

#pragma mark - 

-(instancetype)initWithCards:(NSArray*)cards
{
    if(cards == nil) return nil;
    if([cards count] == 0) return nil;

    self = [super init];
    
    if(self) {
        _cards = [cards copy];
    }
    
    return self;
}

#pragma mark -

+(instancetype)rendererForCards:(NSArray*)cards
{
    return [[self alloc] initWithCards:cards];
}

@end
