//
//  Created by Developer on 2014/05/08.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import <Foundation/Foundation.h>

NSInteger KAMPageRowColumnToArrayIndex(NSInteger pageIndex,
                                       NSInteger rowIndex,
                                       NSInteger columnIndex,
                                       NSInteger numberOfRowsPerPage,
                                       NSInteger numberOfColumnsPerPage);

NSInteger KAMRowColumnToArrayIndex(NSInteger rowIndex,
                                   NSInteger columnIndex,
                                   NSInteger numberOfRowsPerPage,
                                   NSInteger numberOfColumnsPerPage);
