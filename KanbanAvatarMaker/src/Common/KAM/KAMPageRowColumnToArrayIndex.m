//
//  Created by Developer on 2014/05/08.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import "KAMPageRowColumnToArrayIndex.h"

NSInteger KAMPageRowColumnToArrayIndex(NSInteger pageIndex,
                                       NSInteger rowIndex,
                                       NSInteger columnIndex,
                                       NSInteger numberOfRowsPerPage,
                                       NSInteger numberOfColumnsPerPage)
{
    NSInteger arrayIndex = 0;
    
    if(pageIndex > 0) {
        NSInteger segmentsPerPage = (numberOfRowsPerPage * numberOfColumnsPerPage);
        arrayIndex = (pageIndex * segmentsPerPage);
    }
    
    arrayIndex += (rowIndex * numberOfColumnsPerPage);
    
    arrayIndex += columnIndex;
    
    
    return arrayIndex;
}


NSInteger KAMRowColumnToArrayIndex(NSInteger rowIndex,
                                   NSInteger columnIndex,
                                   NSInteger numberOfRowsPerPage,
                                   NSInteger numberOfColumnsPerPage)
{
    return KAMPageRowColumnToArrayIndex(0, rowIndex, columnIndex, numberOfRowsPerPage, numberOfColumnsPerPage);
}
