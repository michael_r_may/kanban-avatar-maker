//
//  Created by Developer on 2014/03/30.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KAMPrintController : NSObject

+(BOOL)isPrintingAvailable;

+(void)printCardWithJobName:(NSString*)jobName pdfCardData:(NSData*)pdfData;

@end
