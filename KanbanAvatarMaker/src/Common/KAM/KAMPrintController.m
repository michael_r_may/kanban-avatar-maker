//
//  Created by Developer on 2014/03/30.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import "MMDebugMacros.h"

#import "KAMPrintController.h"

@implementation KAMPrintController

+(BOOL)isPrintingAvailable
{
    BOOL canPrint = [UIPrintInteractionController isPrintingAvailable];
    return canPrint;
}

+(void)printCardWithJobName:(NSString*)jobName
                pdfCardData:(NSData*)pdfData
{
    if(pdfData == nil) return;
    
    UIPrintInteractionController *printController = [UIPrintInteractionController sharedPrintController];
    
    UIPrintInfo *printInfo = [UIPrintInfo printInfo];
    printInfo.outputType = UIPrintInfoOutputGeneral;
    printInfo.jobName = jobName;
    [printController setPrintInfo:printInfo];
        
    UIPrintInteractionCompletionHandler completionHandler =
    ^(UIPrintInteractionController *printController, BOOL completed,
      NSError *error) {
        if(!completed && error){
            DLog(@"Print failed - domain: %@ error code %ld",
                 error.domain,
                 (long)error.code);
        }
    };
    
    [printController setPrintingItem:pdfData];
    
    [printController presentAnimated:YES completionHandler:completionHandler];
}

@end
