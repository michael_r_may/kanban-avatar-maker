//
//  Created by Developer on 2014/03/30.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KAMUIActivityItemProvider : UIActivityItemProvider 

+(instancetype)activityItemProviderForCards:(NSArray*)cards
                                      title:(NSString*)title;

@end
