//
//  Created by Developer on 2014/03/30.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import <MobileCoreServices/MobileCoreServices.h>

#import "KAMUIActivityItemProvider.h"

#import "KAMCard+Extensions.h"
#import "KAMCardsPDFRenderer.h"
#import "KAMCard+DrawInContext.h"

@interface KAMUIActivityItemProvider () 
@property (nonatomic, copy, readonly) NSString *title;
@property (nonatomic, copy, readonly) NSArray *cards;
@end

@implementation KAMUIActivityItemProvider

#pragma mark - 

-(BOOL)shouldUsePDFRenderingForActivityType:(NSString *)activityType
{
    if ([activityType isEqualToString:UIActivityTypeMail]) return YES;
    
    return NO;
}

#pragma mark - 

// ...the placeholder could be a UIImage object but the actual value could be an NSData object with PDF information.
- (id)activityViewControllerPlaceholderItem:(UIActivityViewController *)activityViewController
{
    return @"";
}

// This method returns the actual data object to be acted on by an activity object
- (id)activityViewController:(UIActivityViewController *)activityViewController
         itemForActivityType:(NSString *)activityType
{
    NSArray *cards = [self cards];
    
    if ([self shouldUsePDFRenderingForActivityType:activityType]) {
        KAMCardsPDFRenderer *renderer = [KAMCardsPDFRenderer rendererForCards:cards];
        NSData *pdfData = [renderer render];
        return pdfData;
    }
    
    {
        static const CGFloat containerSizeWidth = 280.0f;
        static const CGFloat containerSizeHeight = 380.0f;
        UIImage *image = [self renderCard:[cards firstObject] toImageOfSize:CGSizeMake(containerSizeWidth, containerSizeHeight)];
        return image;
    }
}

// Providing the UTI allows services to handle specific data types in appropriate ways
- (NSString *)activityViewController:(UIActivityViewController *)activityViewController
   dataTypeIdentifierForActivityType:(NSString *)activityType
{
    if ([self shouldUsePDFRenderingForActivityType:activityType]) {
        return (NSString*)kUTTypePDF;
    }
    
    return (NSString*)kUTTypeImage;
}

// For activities that support a subject field, returns the subject for the item.
- (NSString *)activityViewController:(UIActivityViewController *)activityViewController
              subjectForActivityType:(NSString *)activityType
{
    NSString *subject = NSLocalizedString(@"ShareStringPrefix", @"");
    subject = [subject stringByAppendingString:[self title]];
    
    return subject;
}

// For activities that support a preview image, returns a thumbnail preview image for the item.
- (UIImage *)activityViewController:(UIActivityViewController *)activityViewController
      thumbnailImageForActivityType:(NSString *)activityType
                      suggestedSize:(CGSize)size
{
    NSArray *cards = [self cards];
    KAMCard *card = [cards firstObject];
    
    return [self renderCard:card toImageOfSize:size];
}

-(UIImage*)renderCard:(KAMCard*)card
        toImageOfSize:(CGSize)size
{
    UIGraphicsBeginImageContext(size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGRect cardRect = {CGPointZero, size};
    [card drawCardInContext:context rect:cardRect];
    
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return viewImage;
}

#pragma mark - 

-(instancetype)initWithCards:(NSArray*)cards
                      title:(NSString*)title
{
    self = [super init];
    
    if(self) {
        _cards = [cards copy];
        _title = [title copy];
    }
    
    return self;
}


#pragma mark -

+(instancetype)activityItemProviderForCards:(NSArray*)cards
                                      title:(NSString*)title
{
    return [[self alloc] initWithCards:cards title:title];
}

@end
