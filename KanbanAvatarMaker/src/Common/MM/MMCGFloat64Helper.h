//
//  Created by Developer on 2014/05/24.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import <tgmath.h>

#ifndef KanbanAvatarMaker_MMCGFloat64Helper_h
#define KanbanAvatarMaker_MMCGFloat64Helper_h

NS_INLINE CGFloat ceil64Helper(CGFloat number)
{
    #if CGFLOAT_IS_DOUBLE
        return ceil(number);
    #else
        return ceilf(number);
    #endif
}

NS_INLINE CGFloat floor64Helper(CGFloat number)
{
#if CGFLOAT_IS_DOUBLE
    return floor(number);
#else
    return floorf(number);
#endif
}

NS_INLINE CGFloat round64Helper(CGFloat number)
{
#if CGFLOAT_IS_DOUBLE
    return round(number);
#else
    return roundf(number);
#endif
}

#endif
