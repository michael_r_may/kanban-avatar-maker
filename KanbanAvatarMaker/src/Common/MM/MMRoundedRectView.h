//
//  Created by Developer on 2014/05/01.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MMRoundedRectView : UIView

@property (nonatomic, strong) UIColor *roundedRectStrokeColor UI_APPEARANCE_SELECTOR;
@property (nonatomic, assign) CGFloat roundedRectStrokeWidth UI_APPEARANCE_SELECTOR;

+(void)drawRoundedRect:(CGRect)rect
           withCorners:(UIRectCorner)corners
                 radii:(CGSize)radii
           strokeWidth:(CGFloat)strokeWidth
           strokeColor:(CGColorRef)strokeColor
             inContext:(CGContextRef)context;

- (void)addRoundedRectWithCornerRadius:(CGFloat)radius;

@end
