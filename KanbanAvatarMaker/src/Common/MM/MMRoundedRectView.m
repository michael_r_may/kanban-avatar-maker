//
//  Created by Developer on 2014/05/01.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import "MMRoundedRectView.h"

@interface MMRoundedRectView ()
@property (nonatomic, assign) UIRectCorner corners;
@property (nonatomic, assign) CGSize cornerRadii;
@end

@implementation MMRoundedRectView

@synthesize cornerRadii = _cornerRadii;

+(void)drawRoundedRect:(CGRect)rect
           withCorners:(UIRectCorner)corners
                 radii:(CGSize)radii
           strokeWidth:(CGFloat)strokeWidth
           strokeColor:(CGColorRef)strokeColor
        inContext:(CGContextRef)context
{
    CGRect innerRect = CGRectInset(rect, (strokeWidth / 2.0f), (strokeWidth / 2.0f));
    UIBezierPath* roundedRectPath = [UIBezierPath bezierPathWithRoundedRect:innerRect
                                                          byRoundingCorners:corners
                                                                cornerRadii:radii];

    CGContextSetStrokeColorWithColor(context, strokeColor);
    CGContextSetLineWidth(context, strokeWidth);
    
    CGContextBeginPath(context);
    CGContextAddPath(context, roundedRectPath.CGPath);
    CGContextDrawPath(context, kCGPathStroke);
}

-(void)drawRect:(CGRect)rect
{
    [super drawRect:rect];

    CGContextRef currentContext = UIGraphicsGetCurrentContext();
    
    [[self class] drawRoundedRect:rect
                      withCorners:self.corners
                            radii:self.cornerRadii
                      strokeWidth:self.roundedRectStrokeWidth
                      strokeColor:self.roundedRectStrokeColor.CGColor
                        inContext:currentContext];
}

- (void)addRoundedRectWithCorners:(UIRectCorner)corners
                           radius:(CGFloat)radius
{
    [self setCorners:corners];
    [self setCornerRadii:CGSizeMake(radius, radius)];
}

#pragma mark -

- (void)addRoundedRectWithCornerRadius:(CGFloat)radius
{
    [self addRoundedRectWithCorners:UIRectCornerAllCorners
                             radius:radius];
}

@end
