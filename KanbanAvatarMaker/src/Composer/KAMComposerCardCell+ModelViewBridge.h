//
//  Created by Developer on 2014/05/10.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import "KAMComposerCardCell.h"

@class KAMCard;

@interface KAMComposerCardCell (ModelViewBridge)

-(void)updateUsingModel:(KAMCard*)card;

-(void)updateModel:(KAMCard*)card;

@end
