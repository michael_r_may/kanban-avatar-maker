//
//  Created by Developer on 2014/05/10.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import "KAMComposerCardCell+ModelViewBridge.h"

#import "KAMCard+Extensions.h"
#import "KAMColor+Extensions.h"

@implementation KAMComposerCardCell (ModelViewBridge)

#pragma mark -

-(void)updateUsingModel:(KAMCard*)card
{
    {   // text
        NSString *name = [card name];
        [[self nameTextField] setText:name];
        
        NSString *title = [card title];
        [[self titleTextField] setText:title];
    }
    
    {   // avatar image
        NSData *imageData = [card image];
        UIImage *image = [UIImage imageWithData:imageData];
        [[self avatarImage] setImage:image];
    }
    
    {
        CGFloat cornerRadius = [card cornerRadius];
        [[self containerView] addRoundedRectWithCornerRadius:cornerRadius];

        CGFloat cardStrokeWidth = [card strokeWidth];
        [[self containerView] setRoundedRectStrokeWidth:cardStrokeWidth];
        
        KAMColor *kamStrokeColor = (KAMColor*)[card strokeColor];
        UIColor *strokeColor = [kamStrokeColor color];
        [self setBorderColor:(UIColor*)strokeColor];
        
        KAMColor *kamBackgroundColor = (KAMColor*)[card backgroundColor];
        UIColor *backgroundColor = [kamBackgroundColor color];
        
        [[self containerView] setBackgroundColor:backgroundColor];
    }
    
    {   // firstLineTextColor
        KAMColor *colorObject = (KAMColor*)[card firstLineTextColor];
        UIColor *color = [colorObject color];
        [[self nameTextField] setTextColor:color];
    }
    
    {   // secondLineTextColor
        KAMColor *colorObject = (KAMColor*)[card secondLineTextColor];
        UIColor *color = [colorObject color];
        [[self titleTextField] setTextColor:color];
    }
    
    [self setNeedsDisplay];
}

-(void)updateModel:(KAMCard*)card
{
    {   // text
        NSString *name = [[self nameTextField] text];
        [card setName:name];
        
        NSString *title = [[self titleTextField] text];
        [card setTitle:title];
    }
    
    {
        UIImage *image = [[self avatarImage] image];
        [card setAvatarImage:image];
    }
    
    {
        UIColor *color = [[self containerView] roundedRectStrokeColor];
        [card setStrokeColorWithColor:color];
    }
    
    {
        UIColor *color = [[self nameTextField] textColor];
        [card setFirstLineTextColorWithColor:color];
    }
    
    {
        UIColor *color = [[self titleTextField] textColor];
        [card setSecondLineTextColorWithColor:color];
    }
}

#pragma mark - specifics



@end
