//
//  Created by Developer on 2014/05/24.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import "KAMComposerCardCell.h"

@interface KAMComposerCardCell (StateInspection)

-(BOOL)isEditingTextFields;

@end
