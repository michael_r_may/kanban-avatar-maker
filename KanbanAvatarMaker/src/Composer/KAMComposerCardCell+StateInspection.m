//
//  Created by Developer on 2014/05/24.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import "KAMComposerCardCell+StateInspection.h"

@implementation KAMComposerCardCell (StateInspection)

-(BOOL)isEditingTextFields
{
    return ([[self nameTextField] isFirstResponder] || [[self titleTextField] isFirstResponder]);
}

@end
