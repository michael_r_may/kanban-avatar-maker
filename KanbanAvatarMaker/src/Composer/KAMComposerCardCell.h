//
//  Created by Developer on 2014/05/10.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MMRoundedRectView.h"

extern NSString *KAMComposerCardCellReuseIdentifier;

typedef NS_ENUM(NSInteger, KAMComposerStates) {
    KAMComposerViewStateNormal = NO,
    KAMComposerViewStatePickingColor = YES
};

@interface KAMComposerCardCell : UICollectionViewCell

@property (nonatomic, strong, readonly) MMRoundedRectView *containerView;
@property (nonatomic, strong, readonly) UIImageView *avatarImage;
@property (nonatomic, strong, readonly) UITextField *nameTextField;
@property (nonatomic, strong, readonly) UITextField *titleTextField;

@property (nonatomic, assign) KAMComposerStates state;

-(BOOL)isNormalState;
-(BOOL)isPickingColorState;
-(KAMComposerStates)toggleState;

-(void)setIsEditable:(BOOL)editable;

-(void)setBorderColor:(UIColor*)color;
-(void)setFirstLineTextColor:(UIColor*)color;
-(void)setSecondLineTextColor:(UIColor*)color;

@end


@interface KAMAvatarUIImageView : UIImageView
@end
