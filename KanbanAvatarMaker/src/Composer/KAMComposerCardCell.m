//
//  Created by Developer on 2014/05/10.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import "KAMComposerCardCell.h"


// also in the xib
NSString *KAMComposerCardCellReuseIdentifier = @"KAMComposerCardCellReuseIdentifier";

@interface KAMComposerCardCell (UITextFieldDelegate)
@property (nonatomic, strong) IBOutlet MMRoundedRectView *containerView;
@property (nonatomic, strong) IBOutlet UIImageView *avatarImage;
@property (nonatomic, strong) IBOutlet UITextField *nameTextField;
@property (nonatomic, strong) IBOutlet UITextField *titleTextField;
@end

@implementation KAMComposerCardCell

#pragma mark -

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

#pragma mark -

-(BOOL)isNormalState
{
    return ([self state] == KAMComposerViewStateNormal);
}

-(BOOL)isPickingColorState
{
    return ([self state] == KAMComposerViewStatePickingColor);
}

-(KAMComposerStates)toggleState
{
    KAMComposerStates state = [self state];
    state = !state;
    [self setState:state];
    
    return state;
}

#pragma mark - 

-(void)setIsEditable:(BOOL)editable
{
    [[self nameTextField] setUserInteractionEnabled:editable];
    [[self titleTextField] setUserInteractionEnabled:editable];
}

-(void)setBorderColor:(UIColor*)color
{
    [[self containerView] setRoundedRectStrokeColor:(UIColor*)color];
    [[self containerView] setNeedsDisplay];
}

-(void)setFirstLineTextColor:(UIColor*)color
{
    [[self nameTextField] setTextColor:color];
}

-(void)setSecondLineTextColor:(UIColor*)color
{
    [[self titleTextField] setTextColor:color];
}

#pragma mark -

-(void)setCardColor:(UIColor*)color
{
    [[self containerView] setRoundedRectStrokeColor:(UIColor*)color];
    [[self containerView] setNeedsDisplay];
}

#pragma mark - 

-(void)awakeFromNib
{
    [super awakeFromNib];
        
    [self setState:KAMComposerViewStateNormal];
}

#pragma mark - 

-(instancetype)initWithFrame:(CGRect)frame
{
    KAMComposerCardCell *view = [[[NSBundle mainBundle] loadNibNamed:@"KAMComposerCardCell" owner:self options:nil] firstObject];
    
    [view setFrame:frame];
    
    return view;
}

@end



@implementation KAMAvatarUIImageView
@end
