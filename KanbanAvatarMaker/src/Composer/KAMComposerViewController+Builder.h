//
//  Created by Developer on 2014/03/30.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import "KAMComposerViewController.h"

@interface KAMComposerViewController (Builder)

+(instancetype)composer;

@end
