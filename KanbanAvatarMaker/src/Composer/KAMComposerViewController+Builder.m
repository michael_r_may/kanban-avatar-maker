//
//  Created by Developer on 2014/03/30.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import "KAMComposerViewController+Builder.h"

#import "KAMCardRepository.h"
#import "KAMSettings.h"

@implementation KAMComposerViewController (Builder)

+(instancetype)composer
{
    KAMCardRepository *cardRepository = [KAMCardRepository repository];
    KAMSettings *settings = [KAMSettings settings];

    return [[self alloc] initWithCardRepository:cardRepository settings:settings];
}

@end
