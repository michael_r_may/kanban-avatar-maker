//
//  Created by Developer on 2014/03/22.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MMDismissBlockProtocol.h"

@class KAMCardRepository;
@class KAMSettings;

@interface KAMComposerViewController : UIViewController <MMDismissBlockProtocol>

-(instancetype)initWithCardRepository:(KAMCardRepository*)cardRepository
                             settings:(KAMSettings*)settings;

@end

