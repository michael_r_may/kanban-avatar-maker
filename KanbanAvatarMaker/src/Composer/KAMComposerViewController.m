//
//  Created by Developer on 2014/03/19.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import "MMDebugMacros.h"

#import <MMImagePickerViewController/MMImagePickerController.h>

#import "KAMPageRowColumnToArrayIndex.h"

#import "KAMComposerViewController.h"
#import "KAMComposerCardCell.h"
#import "KAMComposerCardCell+StateInspection.h"
#import "KAMComposerCardCell+ModelViewBridge.h"

#import "KAMColourPickerViewController.h"
#import "KAMCardsPrintRenderer.h"

#import "KAMPrintController.h"

#import "KAMUIActivityItemProvider.h"

#import "KAMCardRepository.h"
#import "KAMCard+Extensions.h"
#import "KAMColor+Extensions.h"

#import "KAMSettings.h"

#import "MMCGFloat64Helper.h"

@interface KAMComposerViewController () <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, strong, readonly) KAMCardRepository* cardRepository;
@property (nonatomic, strong, readonly) KAMSettings* settings;
@property (nonatomic, strong, readonly) MMImagePickerController* imagePickerController;

@property (nonatomic, weak) IBOutlet UILabel *noContentMessage;

@property (nonatomic, weak) IBOutlet UIView *controlsContainer;
@property (nonatomic, weak) IBOutlet UIButton *addCardButton;
@property (nonatomic, weak) IBOutlet UIButton *deleteCardButton;
@property (nonatomic, weak) IBOutlet UIButton *colorPickingButton;
@property (nonatomic, weak) IBOutlet UIButton *shareButton;
@property (nonatomic, weak) IBOutlet UIButton *printCardButton;

@property (nonatomic, weak) IBOutlet UICollectionView *cardsCollectionView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *cardsCollectionViewTopConstraint;

@end

@implementation KAMComposerViewController

@synthesize dismissBlock;

// class methods to avoid state?
-(NSIndexPath*)indexPathForMostVisibleCard
{
    CGPoint middle = [[self view] center];
    
    UICollectionView *cardsCollectionView = [self cardsCollectionView];
    middle = [cardsCollectionView convertPoint:middle fromView:[self view]];
    
    NSIndexPath *indexPath = [cardsCollectionView indexPathForItemAtPoint:middle];
    
    return indexPath;
}

-(NSInteger)indexForMostVisibleCard
{
    NSIndexPath *indexPath = [self indexPathForMostVisibleCard];
    NSInteger index = KAMRowColumnToArrayIndex([indexPath section], [indexPath row], 1, 1);
    
    return index;
}

-(KAMComposerCardCell*)currentCardCell
{
    NSIndexPath *indexPath = [self indexPathForMostVisibleCard];
    KAMComposerCardCell* currentCard = (KAMComposerCardCell*)[[self cardsCollectionView] cellForItemAtIndexPath:indexPath];
    
    return currentCard;
}

-(KAMCard*)currentCard
{
    NSInteger index = [self indexForMostVisibleCard];
    return [[self cardRepository] cardAtIndex:index];
}

#pragma mark -

-(void)toggleColorPickerState
{
    KAMComposerCardCell* currentCardCell = [self currentCardCell];
    KAMComposerStates state = [currentCardCell toggleState];
    
    BOOL canInteractWithControls = YES;
    if(state == KAMComposerViewStatePickingColor) {
        canInteractWithControls = NO;
    }
    
    [currentCardCell setIsEditable:canInteractWithControls];
    
    BOOL hasCards = ([[self cardRepository] isEmpty] == NO);
    [self updateCardColorPickerControlForState:state enabled:hasCards];
}

#pragma mark -

-(void)updateCardColorPickerControlForState:(KAMComposerStates)state
                                    enabled:(BOOL)enabled
{
    UIButton *colorPickingButton = [self colorPickingButton];
    
    BOOL isSelected = (state == KAMComposerViewStatePickingColor);
    
    [colorPickingButton setSelected:isSelected];
    [colorPickingButton setEnabled:enabled];
}

-(void)updateCardControlsForState
{
    BOOL hasCards = ([[self cardRepository] isEmpty] == NO);
    [[self deleteCardButton] setEnabled:hasCards];
    [[self shareButton] setEnabled:hasCards];
    
    // in case something has changed, like the user has added a printer
    BOOL canPrint = hasCards && [KAMPrintController isPrintingAvailable];
    [[self printCardButton] setEnabled:canPrint];

    KAMComposerCardCell* currentCardCell = [self currentCardCell];
    KAMComposerStates state = [currentCardCell isPickingColorState];
    [self updateCardColorPickerControlForState:state enabled:hasCards];
    
    __strong UILabel *noContentMessage = [self noContentMessage];
    const NSTimeInterval KAMNoMessageAnimationDuration = 0.4f;
    if(hasCards) {
        [[self view] sendSubviewToBack:noContentMessage];
        [noContentMessage setAlpha:0.0f];
    } else {
        [[self view] bringSubviewToFront:noContentMessage];
        
        [UIView animateWithDuration:KAMNoMessageAnimationDuration
                         animations:^{
                             [noContentMessage setAlpha:1.0f];
                         }];
    }
}

#pragma mark -

-(NSString*)cardsDisplayableName
{
    KAMCard *card = [self currentCard];
    return [card displayableName];
}

-(NSArray*)cardsArray
{
    NSArray *cards = [[self cardRepository] allCards];
    return cards;
}

#pragma mark - printing

-(void)printCard
{
    NSArray *cards = [self cardsArray];
    NSString *jobName = [self cardsDisplayableName];
    
    // Get a reference to the singleton iOS printing concierge
    UIPrintInteractionController *printController = [UIPrintInteractionController sharedPrintController];
    
    printController.printPageRenderer = [KAMCardsPrintRenderer rendererForCards:cards];
    
    // Ask for a print job object and configure its settings to tailor the print request
    UIPrintInfo *info = [UIPrintInfo printInfo];
    
    // B&W or color, normal quality output for mixed text, graphics, and images
    info.outputType = UIPrintInfoOutputGeneral;
    
    // Select the job named this in the printer queue to cancel our print request.
    info.jobName = jobName;
    
    // Instruct the printing concierge to use our custom print job settings.
    printController.printInfo = info;
    
    printController.showsPageRange = NO;
    
    // Present the standard iOS Print Panel that allows you to pick the target Printer, number of pages, double-sided, etc.
    [printController presentAnimated:YES completionHandler:nil];
}

#pragma mark -

-(void)presentShareSheet
{
    NSString *jobName = [self cardsDisplayableName];
    NSArray *cards = [self cardsArray];
    
    KAMUIActivityItemProvider *activityItemProvider = [KAMUIActivityItemProvider activityItemProviderForCards:cards title:jobName];
    NSArray *activityItemProviders = [NSArray arrayWithObject:activityItemProvider];
    NSArray *applicationActivities = nil;
    
    UIActivityViewController *shareSheet = [[UIActivityViewController alloc] initWithActivityItems:activityItemProviders
                                                                             applicationActivities:applicationActivities];
    
    [self presentViewController:shareSheet animated:YES completion:nil];
}

#pragma mark - hints

-(void)showColorPickerHintIfNeeded
{
    if([[self settings] shouldShowColorPickerHint]) {
        NSString *hintText = NSLocalizedString(@"ColorPickerHint", @"");
        NSString *dismissButtonText = NSLocalizedString(@"OKText", @"");
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:hintText
                                                       delegate:nil
                                              cancelButtonTitle:nil
                                              otherButtonTitles:dismissButtonText, nil];
        
        [alert show];
    }
}

#pragma mark - actions

-(IBAction)touchUpInsideColorPickingButton:(id)sender
{
    [self toggleColorPickerState];

    [self showColorPickerHintIfNeeded];
}

-(IBAction)touchUpInsideShareButton:(id)sender
{
    [self presentShareSheet];
}

-(IBAction)touchUpInsideActionButton:(id)sender
{
    [self printCard];
}

-(IBAction)touchUpInsideAddNewCardButton:(id)sender
{
    NSInteger index = [self indexForMostVisibleCard];
    [[self cardRepository] addCardAtIndex:index];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    [[self cardsCollectionView] insertItemsAtIndexPaths:@[indexPath]];
}

-(IBAction)touchUpInsideRemoveCardButton:(id)sender
{
    NSInteger index = [self indexForMostVisibleCard];
    [[self cardRepository] removeCardAtIndex:index];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    
    [[self cardsCollectionView] deleteItemsAtIndexPaths:@[indexPath]];
}

#pragma mark - actions

-(void)startAvatarPickerFlow
{
    // we should probably pass a handler here to save the image to the image view and
    // save it to the model too
    [[self imagePickerController] setDismissBlock:^(NSObject* image) {
        if(image == nil) return;
        
        DAssertClass([UIImage class], image);
        
        KAMComposerCardCell* currentCardCell = [self currentCardCell];
        [[currentCardCell avatarImage] setImage:(UIImage*)image];
        
        KAMCard *card = [self currentCard];
        [card setAvatarImage:(UIImage*)image];
    }];
    
    [[self imagePickerController] startFlow];
}

-(void)startBorderColorPickerFlow
{
    KAMColourPickerViewController *controller = [[KAMColourPickerViewController alloc] init];
    
    [controller setDismissBlock:^(NSObject* color) {
        DAssertClass([UIColor class], color);
        
        KAMComposerCardCell* currentCardCell = [self currentCardCell];
        [currentCardCell setBorderColor:(UIColor*)color];
        
        KAMCard *card = [self currentCard];
        [card setStrokeColorWithColor:(UIColor*)color];
        
        [self toggleColorPickerState];
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [self presentViewController:controller animated:YES completion:nil];
}

-(void)startFirstLineColorPickerFlow
{
    KAMColourPickerViewController *controller = [[KAMColourPickerViewController alloc] init];
    
    [controller setDismissBlock:^(NSObject* color) {
        DAssertClass([UIColor class], color);
        
        KAMComposerCardCell* currentCardCell = [self currentCardCell];
        [currentCardCell setFirstLineTextColor:(UIColor*)color];
        
        KAMCard *card = [self currentCard];
        [card setFirstLineTextColorWithColor:(UIColor*)color];
        
        [self toggleColorPickerState];
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [self presentViewController:controller animated:YES completion:nil];
}

-(void)startSecondLineColorPickerFlow
{
    KAMColourPickerViewController *controller = [[KAMColourPickerViewController alloc] init];
    
    [controller setDismissBlock:^(NSObject* color) {
        DAssertClass([UIColor class], color);
        
        KAMComposerCardCell* currentCardCell = [self currentCardCell];
        [currentCardCell setSecondLineTextColor:(UIColor*)color];
        
        KAMCard *card = [self currentCard];
        [card setSecondLineTextColorWithColor:(UIColor*)color];
        
        [self toggleColorPickerState];
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [self presentViewController:controller animated:YES completion:nil];
}

#pragma mark - gestures

-(BOOL)canRespondToGestures
{
    KAMComposerCardCell* currentCardCell = [self currentCardCell];

    if([currentCardCell isEditingTextFields]) return NO;
    
    return YES;
}

// could some of this tap recognition go into the cell, with just the logic here?
-(IBAction)tapGestureRecognized:(id)sender
{
    if([self canRespondToGestures] == NO) return;
    
    UITapGestureRecognizer *tapGestureRecognizer = (UITapGestureRecognizer*)sender;
    KAMComposerCardCell* currentCardCell = [self currentCardCell];
    
    CGPoint point = [tapGestureRecognizer locationInView:[currentCardCell avatarImage]];
    if([[currentCardCell avatarImage] hitTest:point withEvent:nil]) {
        if([currentCardCell isNormalState]) {
            [self startAvatarPickerFlow];
            return;
        }
    }
    
    point = [tapGestureRecognizer locationInView:[currentCardCell nameTextField]];
    if([[currentCardCell nameTextField] pointInside:point withEvent:nil]) {
        if([currentCardCell isPickingColorState]) {
            [self startFirstLineColorPickerFlow];
            return;
        }
    }
    
    point = [tapGestureRecognizer locationInView:[currentCardCell titleTextField]];
    if([[currentCardCell titleTextField] pointInside:point withEvent:nil]) {
        if([currentCardCell isPickingColorState]) {
            [self startSecondLineColorPickerFlow];
            return;
        }
    }
    
    if([currentCardCell isPickingColorState]) {
        [self startBorderColorPickerFlow];
        return;
    }
    
//    point = [tapGestureRecognizer locationInView:[currentCardCell containerView]];
//    if([[currentCardCell containerView] pointInside:point withEvent:nil]) {
//        if([currentCardCell isPickingColorState]) {
//            [self startBorderColorPickerFlow];
//            return;
//        }
//    }
}

#pragma mark - keyboard handling

CGFloat controlsToBottomHeight = 0.0f;

-(CGFloat)controlsToBottomHeight
{
    if(controlsToBottomHeight > 0.0f) return controlsToBottomHeight;
    
    controlsToBottomHeight = CGRectGetHeight([[self view] frame]) - CGRectGetMinY([[self controlsContainer] frame]);
    
    return controlsToBottomHeight;
}

-(CGFloat)frameAdjustmentForKeyboardNotification:(NSNotification*)notification
                                     endFrameKey:(NSString*)endFrameKey
{
    NSDictionary *userInfo = [notification userInfo];
    NSValue *value = [userInfo objectForKey:endFrameKey];
    CGRect endFrameRect = [value CGRectValue];
    CGFloat offsetForControls = [self controlsToBottomHeight];
    
    return (CGRectGetHeight(endFrameRect) - offsetForControls);
}

-(void)animateViewYOffsetBy:(CGFloat)yOffset
                   duration:(NSTimeInterval)duration
                      curve:(UIViewAnimationCurve)curve
{
    [[self view] layoutIfNeeded];
    
    [UIView animateWithDuration:duration
                          delay:0.0f
                        options:(UIViewAnimationOptions)curve
                     animations:^{
                         NSLayoutConstraint *constraint = [self cardsCollectionViewTopConstraint];
                         constraint.constant += yOffset;
                         
                         [[self view] layoutIfNeeded];
                     }
                     completion:nil];
}

-(void)animateViewYOffsetBy:(CGFloat)yOffset
usingValuesFromKeyboardNotificationUserInfo:(NSDictionary*)userInfo
{
    if([self isViewLoaded] == NO) return;
    
    NSTimeInterval duration = 0.5f;
    NSNumber *durationNumber = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    if(durationNumber) duration = [durationNumber doubleValue];
    
    UIViewAnimationCurve curve = UIViewAnimationCurveEaseOut;
    NSNumber *curveNumber = [userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    if(curveNumber) curve = [curveNumber integerValue];
    
    [self animateViewYOffsetBy:yOffset
                      duration:duration
                         curve:curve];
}

-(void)didReceiveUIKeyboardWillShowNotification:(NSNotification*)notification
{
    CGFloat yOffset = [self frameAdjustmentForKeyboardNotification:notification
                                                       endFrameKey:UIKeyboardFrameEndUserInfoKey];
    
    NSDictionary *userInfo = [notification userInfo];
    
    [self animateViewYOffsetBy:-yOffset
usingValuesFromKeyboardNotificationUserInfo:userInfo];
    
    [[self cardsCollectionView] setScrollEnabled:NO];
}

-(void)didReceiveUIKeyboardWillHideNotification:(NSNotification*)notification
{
    CGFloat yOffset = [self frameAdjustmentForKeyboardNotification:notification
                                                       endFrameKey:UIKeyboardFrameBeginUserInfoKey];
    
    NSDictionary *userInfo = [notification userInfo];
    
    [self animateViewYOffsetBy:yOffset
usingValuesFromKeyboardNotificationUserInfo:userInfo];
    
    [self updateModelFromView];
    
    [[self cardsCollectionView] setScrollEnabled:YES];
}

#pragma mark -

#pragma message("Lets try to get rid of this")
-(void)updateModelFromView
{
    KAMCard *card = [self currentCard];
    KAMComposerCardCell* currentCardCell = [self currentCardCell];
    
    [currentCardCell updateModel:card];
    
    [[self cardRepository] saveIfNeeded];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section
{
    NSInteger numberOfItemsInSection = [[self cardRepository] numberOfCards];
    
    DLog(@"numberOfItemsInSection: %ld", (long)numberOfItemsInSection);
    
    return numberOfItemsInSection;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    KAMComposerCardCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:KAMComposerCardCellReuseIdentifier
                                                                          forIndexPath:indexPath];
    
    
    KAMCard *card = [[self cardRepository] cardAtIndex:[indexPath row]];
    
    [cell updateUsingModel:card];
    
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

#pragma mark - scrolling snapping

-(CGFloat)visibleWidthForCell
{
    UICollectionView *collectionView = [self cardsCollectionView];
    UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout *)[collectionView collectionViewLayout];
    DAssert([layout respondsToSelector:@selector(minimumLineSpacing)], @"");
    DAssert([layout respondsToSelector:@selector(itemSize)], @"");
    
    CGFloat visibleWidth = layout.minimumLineSpacing + layout.itemSize.width;
    
    return visibleWidth;
}

// originally from http://markuzweb.blogspot.co.uk/2014/02/uicollectionview-paging-for-smaller-width-cells.html but cut up by me
// NOTE: This delegate method requires you to disable UICollectionView's `pagingEnabled` property.
// This assumes that the values of `layout.sectionInset.left` and
// `layout.sectionInset.right` are the same with `layout.minimumInteritemSpacing`.
// Remember that we're trying to snap to one item at a time. So one
// visible item comprises of its width plus the left margin.
// This also assumed you are using the UICollectionViewFlowLayout
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView
                     withVelocity:(CGPoint)velocity
              targetContentOffset:(inout CGPoint *)targetContentOffset
{
    CGFloat visibleWidthForCell = [self visibleWidthForCell];
    CGPoint currentPoint = *targetContentOffset;
    CGFloat indexOfItemToSnap = round64Helper(currentPoint.x / visibleWidthForCell);
    
    *targetContentOffset = CGPointMake((indexOfItemToSnap * visibleWidthForCell), 0.0f);
}

#pragma mark - keyboard notifications

-(void)startObservingKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didReceiveUIKeyboardWillShowNotification:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didReceiveUIKeyboardWillHideNotification:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

-(void)stopObservingKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

#pragma mark - contentSize observing

// bleugh

static NSString * const KAMContentSizeKeyPath = @"contentSize";

-(void)startObservingContentSizeChanges
{
    [[self cardsCollectionView] addObserver:self
                                 forKeyPath:KAMContentSizeKeyPath
                                    options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld | NSKeyValueObservingOptionPrior)
                                    context:NULL];
}

-(void)stopObservingContentSizeChanges
{
    [[self cardsCollectionView] removeObserver:self
                                    forKeyPath:KAMContentSizeKeyPath];
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    if([self isViewLoaded] == NO) return;
    
    if ([keyPath isEqualToString:KAMContentSizeKeyPath]) {
        [self updateCardControlsForState];
    }
}

#pragma mark -

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	_imagePickerController = [MMImagePickerController imagePickerFlowFromViewController:self];
    
    [[self cardsCollectionView] registerClass:[KAMComposerCardCell class]
                   forCellWithReuseIdentifier:KAMComposerCardCellReuseIdentifier];
    
    [self setEdgesForExtendedLayout:(UIRectEdgeLeft | UIRectEdgeRight)];
    
    [self startObservingContentSizeChanges];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    [self updateCardControlsForState];
}

#pragma mark -

-(instancetype)initWithCardRepository:(KAMCardRepository*)cardRepository
                             settings:(KAMSettings*)settings;
{
    self = [super initWithNibName:nil bundle:nil];
    
    if(self) {
        [self startObservingKeyboardNotifications];
        
        _cardRepository = cardRepository;
        _settings = settings;
    }
    
    return self;
}

-(void)dealloc
{
    [self stopObservingKeyboardNotifications];
    [self stopObservingContentSizeChanges];
}

@end
