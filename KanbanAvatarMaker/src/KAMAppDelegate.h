//
//  KAMAppDelegate.h
//  KanbanAvatarMaker
//
//  Created by Developer on 2014/03/22.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KAMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
