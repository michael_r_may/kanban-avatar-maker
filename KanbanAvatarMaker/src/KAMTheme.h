//
//  Created by Developer on 2014/05/26.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KAMTheme : NSObject

+(void)apply;

@end
