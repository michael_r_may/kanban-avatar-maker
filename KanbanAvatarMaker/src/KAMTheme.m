//
//  Created by Developer on 2014/05/26.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import "KAMTheme.h"

#import "KAMComposerCardCell.h"
#import "KAMComposerViewController.h"

@implementation KAMTheme

+(void)apply
{
    [[UIWindow appearance] setBackgroundColor:[UIColor whiteColor]];
    
    [[KAMComposerCardCell appearance] setBackgroundColor:[UIColor whiteColor]];
    
    UIColor *placeholderGrey = [UIColor colorWithRed:0.780f green:0.780f blue:0.804f alpha:1.0f];
    [[KAMAvatarUIImageView appearanceWhenContainedIn:[KAMComposerCardCell class], nil] setBackgroundColor:placeholderGrey];
    
#pragma message("We need the same blue as the icons")
    // Fixes missing cursor
    [[UITextField appearance] setTintColor:[UIColor blueColor]];
}

@end
