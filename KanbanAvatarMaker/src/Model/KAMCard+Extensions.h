//
//  Created by Developer on 2014/03/30.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import "KAMCard.h"

@interface KAMCard (CreationAndDeletion)

+(instancetype)card;

@end



@interface KAMCard (Extensions)

@property (nonatomic, copy) UIImage *avatarImage;

-(NSString*)displayableName;

-(UIRectCorner)corners;

#pragma mark -

-(UIFont*)firstLineFont;

-(void)setFirstLineTextColorWithColor:(UIColor*)color;

#pragma mark -

-(UIFont*)secondLineFont;

-(void)setSecondLineTextColorWithColor:(UIColor*)color;

#pragma mark - 

-(void)setStrokeColorWithColor:(UIColor*)color;

@end
