//
//  Created by Developer on 2014/03/30.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import "MMDebugMacros.h"

#import "KAMCard+Extensions.h"
#import "KAMColor+Extensions.h"

#import <CoreData+MagicalRecord.h>

#import <MagicalRecord/NSManagedObjectContext+MagicalRecord.h>
#import <MagicalRecord/NSManagedObjectContext+MagicalSaves.h>

@implementation KAMCard (Extensions)

-(NSString*)displayableName
{
    return [self name];
}

#pragma mark -

// hooks in case were ever want to make these configurable

-(UIRectCorner)corners
{
    return UIRectCornerAllCorners;
}

#pragma mark -

-(UIFont*)firstLineFont
{
    CGFloat firstLineFontSize = [self firstLineFontSize];
    return [UIFont systemFontOfSize:firstLineFontSize];
}

-(void)setFirstLineTextColorWithColor:(UIColor*)color
{
    KAMColor *colorObject = (KAMColor*)[self firstLineTextColor];
    [colorObject setColor:color];
}

#pragma mark -

-(UIFont*)secondLineFont
{
    CGFloat secondLineFontSize = [self secondLineFontSize];
    return [UIFont systemFontOfSize:secondLineFontSize];
}

-(void)setSecondLineTextColorWithColor:(UIColor*)color
{
    KAMColor *colorObject = (KAMColor*)[self secondLineTextColor];
    [colorObject setColor:color];
}

#pragma mark - 

-(UIImage*)avatarImage
{
    NSData *imageData = [self image];
    UIImage *image = [UIImage imageWithData:imageData];
    
    return image;
}

-(void)setAvatarImage:(UIImage *)image
{
    NSData *imageData = UIImagePNGRepresentation(image);
    
    [self setImage:imageData];
}

#pragma mark - 

-(void)setStrokeColorWithColor:(UIColor*)color
{
    KAMColor *colorObject = (KAMColor*)[self strokeColor];
    [colorObject setColor:(UIColor*)color];
}

#pragma mark -

+(instancetype)card
{
    KAMCard *card = [KAMCard MR_createEntity];

    [card setImage:nil];
    [card setName:@""];
    [card setTitle:@""];

#pragma mark ("naming inconsistency between fill and background")
    [card setCornerRadius:10.0f];
    [card setStrokeWidth:2.0f];
    [card setStrokeColor:[KAMColor defaultCardStrokeColor]];
    
    [card setBackgroundColor:[KAMColor defaultCardFillColor]];
    [card setFirstLineBackgroundColor:[KAMColor defaultCardStrokeColor]];
    [card setFirstLineTextColor:[KAMColor defaultCardStrokeColor]];
    [card setSecondLineBackgroundColor:[KAMColor defaultCardStrokeColor]];
    [card setSecondLineTextColor:[KAMColor defaultCardStrokeColor]];
    
    return card;
}

@end
