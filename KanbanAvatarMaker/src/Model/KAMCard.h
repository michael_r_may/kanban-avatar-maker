//
//  KAMCard.h
//  KanbanAvatarMaker
//
//  Created by Developer on 2014/05/04.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class KAMColor;

@interface KAMCard : NSManagedObject

@property (nonatomic, retain) NSData * image;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * title;
@property (nonatomic) float strokeWidth;
@property (nonatomic) float cornerRadius;
@property (nonatomic) float firstLineFontSize;
@property (nonatomic) float secondLineFontSize;
@property (nonatomic, retain) KAMColor *backgroundColor;
@property (nonatomic, retain) KAMColor *strokeColor;
@property (nonatomic, retain) KAMColor *firstLineBackgroundColor;
@property (nonatomic, retain) KAMColor *firstLineTextColor;
@property (nonatomic, retain) KAMColor *secondLineBackgroundColor;
@property (nonatomic, retain) KAMColor *secondLineTextColor;

@end
