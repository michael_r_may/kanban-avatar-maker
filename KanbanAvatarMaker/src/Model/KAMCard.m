//
//  KAMCard.m
//  KanbanAvatarMaker
//
//  Created by Developer on 2014/05/04.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import "KAMCard.h"
#import "KAMColor.h"


@implementation KAMCard

@dynamic image;
@dynamic name;
@dynamic title;
@dynamic strokeWidth;
@dynamic cornerRadius;
@dynamic firstLineFontSize;
@dynamic secondLineFontSize;
@dynamic backgroundColor;
@dynamic strokeColor;
@dynamic firstLineBackgroundColor;
@dynamic firstLineTextColor;
@dynamic secondLineBackgroundColor;
@dynamic secondLineTextColor;

@end
