//
//  Created by Developer on 2014/05/17.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import "KAMCardIndex.h"

@interface KAMCardIndex (Extensions)

+(instancetype)currentCardIndex;

@end


@interface KAMCardIndex (Creation)

+(instancetype)cardIndex;

@end


@interface KAMCardIndex (ObjectAccessHelpers)

-(NSArray*)allCards;

-(KAMCard*)cardAtIndex:(NSUInteger)index;

-(void)insertCard:(KAMCard*)card atIndex:(NSUInteger)index;

-(void)removeCardAtIndex:(NSUInteger)index;

-(NSUInteger)numberOfCards;

-(BOOL)isEmpty;

@end
