//
//  Created by Developer on 2014/05/17.
//  Copyright (c) 2014 Michael May. All rights reserved.
//
//  Note: Make sure you do everything on [NSManagedObjectContext MR_defaultContext] or
//  you will have sync problems because your contexts get stale. Deleted items will
//  still be counted when you call methods on different threads and the like

#import "MMDebugMacros.h"

#import "KAMCardIndex+Extensions.h"

#import <CoreData+MagicalRecord.h>

#import <MagicalRecord/NSManagedObjectContext+MagicalRecord.h>
#import <MagicalRecord/NSManagedObjectContext+MagicalSaves.h>

@implementation KAMCardIndex (Extensions)

+(instancetype)currentCardIndex
{
    return [KAMCardIndex MR_findFirst];
}

@end


@implementation KAMCardIndex (Creation)

+(instancetype)cardIndex
{
    NSOrderedSet *set = [[NSOrderedSet alloc] init];
    KAMCardIndex *cardIndex = [KAMCardIndex MR_createEntity];
    
    [cardIndex setOrderedCards:set];
    
    return cardIndex;
}

@end


@implementation KAMCardIndex (ObjectAccessHelpers)

-(NSOrderedSet*)allCardsInOrder
{
    return [self orderedCards];
}

-(NSArray*)allCards
{
    return [[self allCardsInOrder] array];
}

-(KAMCard*)cardAtIndex:(NSUInteger)index
{
    NSOrderedSet *orderedCards = [self allCardsInOrder];

    DAssert(index < [orderedCards count], @"");
    
    KAMCard* card = nil;
    
    if(index < [orderedCards count]) {
        card = [orderedCards objectAtIndex:index];
    }
    
    return card;
}

-(void)insertCard:(KAMCard*)card atIndex:(NSUInteger)index
{
    NSOrderedSet *orderedCards = [self allCardsInOrder];

    DAssert(index == 0 || index < [orderedCards count], @"");
    
    NSMutableOrderedSet *newSet = [[self orderedCards] mutableCopy];
    [newSet insertObject:card atIndex:index];
    [self setOrderedCards:newSet];
}

// weve set cascade delete so that should clear out the KAMCard too
-(void)removeCardAtIndex:(NSUInteger)index
{
    NSMutableOrderedSet *newSet = [[self orderedCards] mutableCopy];
    [newSet removeObjectAtIndex:index];
    [self setOrderedCards:newSet];
}

-(NSUInteger)numberOfCards
{
    NSOrderedSet *cardIndex = [self allCardsInOrder];
    
    return [cardIndex count];
}

-(BOOL)isEmpty
{
    return ([self numberOfCards] == 0);
}

#pragma mark - bug fixes

static NSString *const kItemsKey = @"orderedCards";

- (void)insertObject:(KAMCard *)value inOrderedCardsAtIndex:(NSUInteger)idx
{
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:kItemsKey];

    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self mutableOrderedSetValueForKey:kItemsKey]];

    [tmpOrderedSet insertObject:value atIndex:idx];

    [self setPrimitiveValue:tmpOrderedSet forKey:kItemsKey];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:kItemsKey];
}

- (void)removeOrderedCardsAtIndexes:(NSIndexSet *)indexes
{
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:kItemsKey];
    
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self mutableOrderedSetValueForKey:kItemsKey]];
    [tmpOrderedSet removeObjectsAtIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:kItemsKey];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:kItemsKey];
}

/*
 http://stackoverflow.com/questions/7385439/exception-thrown-in-nsorderedset-generated-accessors
 
 
 - (void)removeObjectFromOrderedCardsAtIndex:(NSUInteger)idx
 {
 NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
 [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:kItemsKey];
 NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self mutableOrderedSetValueForKey:kItemsKey]];
 [tmpOrderedSet removeObjectAtIndex:idx];
 [self setPrimitiveValue:tmpOrderedSet forKey:kItemsKey];
 [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:kItemsKey];
 }
 
 - (void)removeObjectFrom<#Property#>AtIndex:(NSUInteger)idx {
 NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
 [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:kItemsKey];
 NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self mutableOrderedSetValueForKey:kItemsKey]];
 [tmpOrderedSet removeObjectAtIndex:idx];
 [self setPrimitiveValue:tmpOrderedSet forKey:kItemsKey];
 [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:kItemsKey];
 }
 
 - (void)insert<#Property#>:(NSArray *)values atIndexes:(NSIndexSet *)indexes {
 [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:kItemsKey];
 NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self mutableOrderedSetValueForKey:kItemsKey]];
 [tmpOrderedSet insertObjects:values atIndexes:indexes];
 [self setPrimitiveValue:tmpOrderedSet forKey:kItemsKey];
 [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:kItemsKey];
 }
 
 - (void)replaceObjectIn<#Property#>AtIndex:(NSUInteger)idx withObject:(<#Type#> *)value {
 NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
 [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:kItemsKey];
 NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self mutableOrderedSetValueForKey:kItemsKey]];
 [tmpOrderedSet replaceObjectAtIndex:idx withObject:value];
 [self setPrimitiveValue:tmpOrderedSet forKey:kItemsKey];
 [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:kItemsKey];
 }
 
 - (void)replace<#Property#>AtIndexes:(NSIndexSet *)indexes with<#Property#>:(NSArray *)values {
 [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:kItemsKey];
 NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self mutableOrderedSetValueForKey:kItemsKey]];
 [tmpOrderedSet replaceObjectsAtIndexes:indexes withObjects:values];
 [self setPrimitiveValue:tmpOrderedSet forKey:kItemsKey];
 [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:kItemsKey];
 }
 
 - (void)add<#Property#>Object:(<#Type#> *)value {
 NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self mutableOrderedSetValueForKey:kItemsKey]];
 NSUInteger idx = [tmpOrderedSet count];
 NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
 [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:kItemsKey];
 [tmpOrderedSet addObject:value];
 [self setPrimitiveValue:tmpOrderedSet forKey:kItemsKey];
 [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:kItemsKey];
 }
 
 - (void)remove<#Property#>Object:(<#Type#> *)value {
 NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self mutableOrderedSetValueForKey:kItemsKey]];
 NSUInteger idx = [tmpOrderedSet indexOfObject:value];
 if (idx != NSNotFound) {
 NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
 [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:kItemsKey];
 [tmpOrderedSet removeObject:value];
 [self setPrimitiveValue:tmpOrderedSet forKey:kItemsKey];
 [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:kItemsKey];
 }
 }
 
 - (void)add<#Property#>:(NSOrderedSet *)values {
 NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self mutableOrderedSetValueForKey:kItemsKey]];
 NSMutableIndexSet *indexes = [NSMutableIndexSet indexSet];
 NSUInteger valuesCount = [values count];
 NSUInteger objectsCount = [tmpOrderedSet count];
 for (NSUInteger i = 0; i < valuesCount; ++i) {
 [indexes addIndex:(objectsCount + i)];
 }
 if (valuesCount > 0) {
 [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:kItemsKey];
 [tmpOrderedSet addObjectsFromArray:[values array]];
 [self setPrimitiveValue:tmpOrderedSet forKey:kItemsKey];
 [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:kItemsKey];
 }
 }
 
 - (void)remove<#Property#>:(NSOrderedSet *)values {
 NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self mutableOrderedSetValueForKey:kItemsKey]];
 NSMutableIndexSet *indexes = [NSMutableIndexSet indexSet];
 for (id value in values) {
 NSUInteger idx = [tmpOrderedSet indexOfObject:value];
 if (idx != NSNotFound) {
 [indexes addIndex:idx];
 }
 }
 if ([indexes count] > 0) {
 [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:kItemsKey];
 [tmpOrderedSet removeObjectsAtIndexes:indexes];
 [self setPrimitiveValue:tmpOrderedSet forKey:kItemsKey];
 [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:kItemsKey];
 }
 }
*/

@end
