//
//  Created by Developer on 2014/05/17.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class KAMCard;

@interface KAMCardIndex : NSManagedObject

@property (nonatomic, retain) NSOrderedSet *orderedCards;
@end

@interface KAMCardIndex (CoreDataGeneratedAccessors)

- (void)insertObject:(KAMCard *)value inOrderedCardsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromOrderedCardsAtIndex:(NSUInteger)idx;
- (void)insertOrderedCards:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeOrderedCardsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInOrderedCardsAtIndex:(NSUInteger)idx withObject:(KAMCard *)value;
- (void)replaceOrderedCardsAtIndexes:(NSIndexSet *)indexes withOrderedCards:(NSArray *)values;
- (void)addOrderedCardsObject:(KAMCard *)value;
- (void)removeOrderedCardsObject:(KAMCard *)value;
- (void)addOrderedCards:(NSOrderedSet *)values;
- (void)removeOrderedCards:(NSOrderedSet *)values;
@end
