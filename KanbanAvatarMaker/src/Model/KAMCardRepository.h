//
//  Created by Developer on 2014/03/30.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import <Foundation/Foundation.h>

@class KAMCard;

@interface KAMCardRepository : NSObject

@property (nonatomic, assign) BOOL shouldDeleteOnCleanUp;

-(NSArray*)allCards;

-(KAMCard*)cardAtIndex:(NSUInteger)index;

-(NSUInteger)numberOfCards;

-(BOOL)isEmpty;

#pragma mark - 

+(instancetype)repository;

@end


@interface KAMCardRepository (Mutability)

-(KAMCard*)addCardAtIndex:(NSInteger)index;

-(void)removeCardAtIndex:(NSInteger)index;

-(void)saveIfNeeded;

@end
