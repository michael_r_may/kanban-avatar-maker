//
//  Created by Developer on 2014/03/30.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import "MMDebugMacros.h"

#import <CoreData/CoreData.h>

#import <CoreData+MagicalRecord.h>
#import <NSManagedObject+MagicalFinders.h>
#import <NSPersistentStoreCoordinator+MagicalRecord.h>

#import "KAMCardRepository.h"

#import "KAMCardIndex+Extensions.h"
#import "KAMCard+Extensions.h"

@interface KAMCardRepository ()
@property (nonatomic, strong, readonly) KAMCardIndex *cardIndex;
@property (nonatomic, strong) NSURL *storeURL;
@end

@implementation KAMCardRepository (Mutability)

-(KAMCard*)addCardAtIndex:(NSInteger)index
{
    KAMCard *newCard = [self createNewCard];
    
    [[self cardIndex] insertCard:newCard atIndex:index];
    
    [self saveIfNeeded];
    
    return newCard;
}

-(void)removeCardAtIndex:(NSInteger)index
{
    KAMCard *card = [[self cardIndex] cardAtIndex:index];
    
    [[self cardIndex] removeCardAtIndex:index];
    
    [self deleteCard:card];
    
    [self saveIfNeeded];
}

#pragma mark -

-(KAMCardIndex*)createNewCardIndex
{
    KAMCardIndex *cardIndex = [KAMCardIndex cardIndex];
    [self saveIfNeeded];
    
    return cardIndex;
}

-(KAMCard*)createNewCard
{
    KAMCard *emptyCard = [KAMCard card];
    [self saveIfNeeded];

    return emptyCard;
}

-(void)deleteCard:(KAMCard*)card
{
    [card MR_deleteEntity];
    [self saveIfNeeded];
}

#pragma mark - 

- (void)saveIfNeeded {
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        if (success) {
            NSLog(@"You successfully saved your context.");
        } else if (error) {
            NSLog(@"Error saving context: %@", error.description);
        }
    }];
}

@end


@implementation KAMCardRepository

@synthesize shouldDeleteOnCleanUp;

-(KAMCard*)cardAtIndex:(NSUInteger)index
{
    return [[self cardIndex] cardAtIndex:index];
}

-(NSArray*)allCards
{
    NSArray *cards = [[self cardIndex] allCards];
    return cards;
}

-(NSUInteger)numberOfCards
{
    return [[self cardIndex] numberOfCards];
}

-(BOOL)isEmpty
{
    return ([self numberOfCards] == 0);
}

#pragma mark - 

-(instancetype)init
{
    self = [super init];
    
    if(self) {
        [MagicalRecord setupCoreDataStackWithAutoMigratingSqliteStoreNamed:@"kam"];

        _storeURL = [NSPersistentStore MR_urlForStoreName:@"kam"];
        
        _cardIndex = [KAMCardIndex currentCardIndex];
        
        if(_cardIndex == nil) {
            _cardIndex = [self createNewCardIndex];
        }
    }
    
    return self;
}

-(void)dealloc
{
    [MagicalRecord cleanUp];
    
    if(self.shouldDeleteOnCleanUp) {
        NSError *error = nil;
        
        [[NSFileManager defaultManager] removeItemAtURL:self.storeURL
                                                  error:&error];
    }
}

#pragma mark -

+(instancetype)repository
{
    return [[self alloc] init];
}

@end

