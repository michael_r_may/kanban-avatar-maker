//
//  Created by Developer on 2014/04/01.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import "KAMColor.h"

@interface KAMColor (Extensions)

@property (nonatomic, copy) UIColor* color;

+(instancetype)defaultCardStrokeColor;
+(instancetype)defaultCardFillColor;
@end
