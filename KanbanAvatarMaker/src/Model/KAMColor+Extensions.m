//
//  Created by Developer on 2014/04/01.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import "KAMColor+Extensions.h"

#import <CoreData+MagicalRecord.h>

#import "MMCGFloat64Helper.h"

@implementation KAMColor (Extensions)

-(void)setColor:(UIColor *)color
{
    CGFloat redComponent;
    CGFloat greenComponent;
    CGFloat blueComponent;
    CGFloat alphaComponent;
    
    [color getRed:&redComponent
            green:&greenComponent
             blue:&blueComponent
            alpha:&alphaComponent];
    
    [self setColorRed:redComponent];
    [self setColorGreen:greenComponent];
    [self setColorBlue:blueComponent];
}

-(UIColor*)color
{
    CGFloat redColor = (CGFloat)[self colorRed];
    CGFloat greenColor = (CGFloat)[self colorGreen];
    CGFloat blueColor = (CGFloat)[self colorBlue];
    CGFloat alpha = 1.0f;
    
    return [UIColor colorWithRed:redColor green:greenColor blue:blueColor alpha:alpha];
}

#pragma mark - 

+(instancetype)defaultCardStrokeColor
{
    KAMColor *color = [KAMColor MR_createEntity];
    
    [color setColor:[UIColor blackColor]];
    
    return color;
}

+(instancetype)defaultCardFillColor
{
    KAMColor *color = [KAMColor MR_createEntity];
    
    [color setColor:[UIColor whiteColor]];
    
    return color;
}

@end
