//
//  KAMColor.h
//  KanbanAvatarMaker
//
//  Created by Developer on 2014/05/24.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface KAMColor : NSManagedObject

@property (nonatomic) double colorBlue;
@property (nonatomic) double colorGreen;
@property (nonatomic) double colorRed;

@end
