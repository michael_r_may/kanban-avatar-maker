//
//  KAMColor.m
//  KanbanAvatarMaker
//
//  Created by Developer on 2014/05/24.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import "KAMColor.h"


@implementation KAMColor

@dynamic colorBlue;
@dynamic colorGreen;
@dynamic colorRed;

@end
