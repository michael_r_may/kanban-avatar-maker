//
//  Created by Developer on 2014/05/26.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KAMSettings : NSObject

@property (nonatomic, assign, readonly) BOOL shouldShowColorPickerHint;

+(instancetype)settings;

@end
