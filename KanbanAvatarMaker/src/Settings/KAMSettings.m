//
//  Created by Developer on 2014/05/26.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import "KAMSettings.h"

@interface KAMSettings ()
@property (nonatomic, strong, readonly) NSUserDefaults *storage;
@end


@implementation KAMSettings

static NSString *KAMSettingsShowColorPickerHintKey = @"KAMSettingsShowColorPickerHintKey";

-(BOOL)shouldShowColorPickerHint
{
    NSUserDefaults *storage = [self storage];
    NSObject *objectForKey = [storage objectForKey:KAMSettingsShowColorPickerHintKey];
    
    BOOL shouldShowColorPickerHint = (objectForKey == nil);
    [self setShouldShowColorPickerHint:NO];
    
    return shouldShowColorPickerHint;
}

-(void)setShouldShowColorPickerHint:(BOOL)shouldShowColorPickerHint
{
    NSUserDefaults *storage = [self storage];
    
    [storage setBool:NO forKey:KAMSettingsShowColorPickerHintKey];
}

#pragma mark -

-(instancetype)init
{
    self = [super init];
    
    if(self) {
        _storage = [NSUserDefaults standardUserDefaults];
    }
    
    return self;
}

#pragma mark -

+(instancetype)settings
{
    return [[self alloc] init];
}

@end
