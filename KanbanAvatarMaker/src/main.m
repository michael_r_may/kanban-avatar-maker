//
//  main.m
//  KanbanAvatarMaker
//
//  Created by Developer on 2014/03/22.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "KAMAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([KAMAppDelegate class]));
    }
}
