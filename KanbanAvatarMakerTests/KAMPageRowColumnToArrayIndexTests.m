//
//  Created by Developer on 2014/05/08.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "KAMPageRowColumnToArrayIndex.h"

@interface KAMPageRowColumnToArrayIndexTests : XCTestCase

@end

@implementation KAMPageRowColumnToArrayIndexTests

NSInteger numberOfColumnsPerPage = 2;
NSInteger numberOfRowsPerPage = 2;

#pragma mark - 

// [*][]
// [][]
-(void)testThatKAMPageRowColumnToArrayIndexForPage0Row0Column0
{
    // given
    
    // when
    NSInteger pageIndex = 0;
    NSInteger rowIndex = 0;
    NSInteger columnIndex = 0;
    
    NSInteger arrayIndex = KAMPageRowColumnToArrayIndex(pageIndex, rowIndex, columnIndex, numberOfRowsPerPage, numberOfColumnsPerPage);
    
    // then
    NSInteger expectedIndex = 0;
    XCTAssertEqual(arrayIndex, expectedIndex, @"");
}

// [][*]
// [][]
-(void)testThatKAMPageRowColumnToArrayIndexForPage0Row0Column1
{
    // given
    
    // when
    NSInteger pageIndex = 0;
    NSInteger rowIndex = 0;
    NSInteger columnIndex = 1;
    
    NSInteger arrayIndex = KAMPageRowColumnToArrayIndex(pageIndex, rowIndex, columnIndex, numberOfRowsPerPage, numberOfColumnsPerPage);
    
    // then
    NSInteger expectedIndex = 1;
    XCTAssertEqual(arrayIndex, expectedIndex, @"");
}

// [][]
// [*][]
-(void)testThatKAMPageRowColumnToArrayIndexForPage0Row1Column0
{
    // given
    
    // when
    NSInteger pageIndex = 0;
    NSInteger rowIndex = 1;
    NSInteger columnIndex = 0;
    
    NSInteger arrayIndex = KAMPageRowColumnToArrayIndex(pageIndex, rowIndex, columnIndex, numberOfRowsPerPage, numberOfColumnsPerPage);
    
    // then
    NSInteger expectedIndex = 2;
    XCTAssertEqual(arrayIndex, expectedIndex, @"");
}

// [][]
// [][*]
-(void)testThatKAMPageRowColumnToArrayIndexForPage0Row1Column1
{
    // given
    
    // when
    NSInteger pageIndex = 0;
    NSInteger rowIndex = 1;
    NSInteger columnIndex = 1;
    
    NSInteger arrayIndex = KAMPageRowColumnToArrayIndex(pageIndex, rowIndex, columnIndex, numberOfRowsPerPage, numberOfColumnsPerPage);
    
    // then
    NSInteger expectedIndex = 3;
    XCTAssertEqual(arrayIndex, expectedIndex, @"");
}

#pragma mark - page 1

// [][]
// [][]
//  -
// [*][]
// [][]
-(void)testThatKAMPageRowColumnToArrayIndexForPage1Row0Column0
{
    // given
    
    // when
    NSInteger pageIndex = 1;
    NSInteger rowIndex = 0;
    NSInteger columnIndex = 0;
    
    NSInteger arrayIndex = KAMPageRowColumnToArrayIndex(pageIndex, rowIndex, columnIndex, numberOfRowsPerPage, numberOfColumnsPerPage);
    
    // then
    NSInteger expectedIndex = 4;
    XCTAssertEqual(arrayIndex, expectedIndex, @"");
}

// [][]
// [][]
//  -
// [][*]
// [][]
-(void)testThatKAMPageRowColumnToArrayIndexForPage1Row0Column1
{
    // given
    
    // when
    NSInteger pageIndex = 1;
    NSInteger rowIndex = 0;
    NSInteger columnIndex = 1;
    
    NSInteger arrayIndex = KAMPageRowColumnToArrayIndex(pageIndex, rowIndex, columnIndex, numberOfRowsPerPage, numberOfColumnsPerPage);
    
    // then
    NSInteger expectedIndex = 5;
    XCTAssertEqual(arrayIndex, expectedIndex, @"");
}

// [][]
// [][]
//  -
// [][]
// [*][]
-(void)testThatKAMPageRowColumnToArrayIndexForPage1Row1Column0
{
    // given
    
    // when
    NSInteger pageIndex = 1;
    NSInteger rowIndex = 1;
    NSInteger columnIndex = 0;
    
    NSInteger arrayIndex = KAMPageRowColumnToArrayIndex(pageIndex, rowIndex, columnIndex, numberOfRowsPerPage, numberOfColumnsPerPage);
    
    // then
    NSInteger expectedIndex = 6;
    XCTAssertEqual(arrayIndex, expectedIndex, @"");
}

// [][]
// [][]
//  -
// [][]
// [][*]
-(void)testThatKAMPageRowColumnToArrayIndexForPage1Row1Column1
{
    // given
    
    // when
    NSInteger pageIndex = 1;
    NSInteger rowIndex = 1;
    NSInteger columnIndex = 1;
    
    NSInteger arrayIndex = KAMPageRowColumnToArrayIndex(pageIndex, rowIndex, columnIndex, numberOfRowsPerPage, numberOfColumnsPerPage);
    
    // then
    NSInteger expectedIndex = 7;
    XCTAssertEqual(arrayIndex, expectedIndex, @"");
}

#pragma mark - page 3

// [][]
// [][]
//  -
// [][]
// [][]
//  -
// [][]
// [][*]
-(void)testThatKAMPageRowColumnToArrayIndexForPage2Row1Column1
{
    // given
    
    // when
    NSInteger pageIndex = 2;
    NSInteger rowIndex = 1;
    NSInteger columnIndex = 1;
    
    NSInteger arrayIndex = KAMPageRowColumnToArrayIndex(pageIndex, rowIndex, columnIndex, numberOfRowsPerPage, numberOfColumnsPerPage);
    
    // then
    NSInteger expectedIndex = 11;
    XCTAssertEqual(arrayIndex, expectedIndex, @"");
}


#pragma mark - larger column count page 3

// [][][][]
// [][][][]
//  -
// [][][][]
// [][][][]
//  -
// [][][][]
// [][*][][]
-(void)testThatKAMPageRowColumnToArrayIndexForPage2Row1Column2ColumnCount4
{
    // given
    NSInteger pageIndex = 2;
    NSInteger rowIndex = 1;
    NSInteger columnIndex = 1;
    
    numberOfColumnsPerPage = 4;
    
    // when
    NSInteger arrayIndex = KAMPageRowColumnToArrayIndex(pageIndex, rowIndex, columnIndex, numberOfRowsPerPage, numberOfColumnsPerPage);
    
    // then
    NSInteger expectedIndex = 21;
    XCTAssertEqual(arrayIndex, expectedIndex, @"");
}

@end
