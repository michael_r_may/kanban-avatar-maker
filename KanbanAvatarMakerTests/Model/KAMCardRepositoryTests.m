//
//  Created by Developer on 2014/06/08.
//  Copyright (c) 2014 Michael May. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "KAMCardRepository.h"
#import "KAMCard.h"
#import "KAMCard+Extensions.h"
#import "KAMColor+Extensions.h"

@interface KAMCardRepository (ForTestingOnly)
+(instancetype)repositoryWithStoreName:(NSString*)storeName;
@end

@interface KAMCardRepositoryTests : XCTestCase
@property (nonatomic, strong) NSURL *storeURL;
@end

@implementation KAMCardRepositoryTests

- (void)setUp
{
    [super setUp];
}

- (void)tearDown
{
    [super tearDown];
}

#pragma mark - utility

-(KAMCardRepository*)repo
{
    KAMCardRepository *repo = [KAMCardRepository repository];
    
    repo.shouldDeleteOnCleanUp = YES;
    
    return repo;
}

-(void)assertThatKAMColorIsBlack:(KAMColor*)color
{
    XCTAssertEqual([color colorBlue], 0.0f, @"");
    XCTAssertEqual([color colorGreen], 0.0f, @"");
    XCTAssertEqual([color colorRed], 0.0f, @"");
}

-(void)addANewCardWithABlueStroke:(KAMCardRepository*)repo
{
    NSUInteger cardCount = [repo numberOfCards];
    
    KAMCard *card = [repo addCardAtIndex:0];
    UIColor *colorToSet = [UIColor colorWithRed:0.0f green:0.0f blue:1.0 alpha:1.0f];
    [card setStrokeColorWithColor:colorToSet];
    
    XCTAssertEqual([repo numberOfCards], cardCount + 1, @"");
}

-(void)addANewCardWithADefaultStroke:(KAMCardRepository*)repo
{
    NSUInteger cardCount = [repo numberOfCards];
    
    [repo addCardAtIndex:0];
    
    XCTAssertEqual([repo numberOfCards], cardCount + 1, @"");
}

-(void)removeFirstCard:(KAMCardRepository*)repo
{
    NSUInteger cardCount = [repo numberOfCards];
    
    [repo removeCardAtIndex:0];
    
    XCTAssertEqual([repo numberOfCards], cardCount - 1, @"");
}

#pragma mark -

-(void)testTheRepositoryFactory
{
    // given
    
    // when
    KAMCardRepository *repo = [self repo];
    
    // then
    XCTAssertNotNil(repo, @"");
}

-(void)testThatAddCardAtIndexInAnEmptyRepoWorks
{
    // given
    KAMCardRepository *repo = [self repo];
    
    // when
    KAMCard *card = [repo addCardAtIndex:0];
    
    // then
    XCTAssertNotNil(card, @"");
}

-(void)testThatANewCardHasABorderColor
{
    // given
    KAMCardRepository *repo = [self repo];
    
    // when
    KAMCard *card = [repo addCardAtIndex:0];
    KAMColor *color = [card strokeColor];
    
    // then
    XCTAssertNotNil(color, @"");
}

-(void)testThatANewCardHasABlackBorderColor
{
    // given
    KAMCardRepository *repo = [self repo];
    
    // when
    KAMCard *card = [repo addCardAtIndex:0];
    KAMColor *color = [card strokeColor];
    
    // then
    [self assertThatKAMColorIsBlack:color];
}

-(void)testThatAddingANewCardChangesTheCardCount
{
    // given
    KAMCardRepository *repo = [self repo];
    XCTAssertEqual([repo numberOfCards], (NSUInteger)0, @"");
    
    // when
    KAMCard *card = [repo addCardAtIndex:0];
    XCTAssertNotNil(card, @"");
    
    // then
    XCTAssertEqual([repo numberOfCards], (NSUInteger)1, @"");
}

-(void)testThatAddingAnotherCardChangesTheCardCount
{
    // given
    KAMCardRepository *repo = [self repo];
    KAMCard *card = [repo addCardAtIndex:0];
    XCTAssertEqual([repo numberOfCards], (NSUInteger)1, @"");
    
    // when
    [repo addCardAtIndex:0];
    XCTAssertNotNil(card, @"");
    
    // then
    XCTAssertEqual([repo numberOfCards], (NSUInteger)2, @"");
}

#pragma mark -

-(void)testSettingTheStrokeColor
{
    // given
    KAMCardRepository *repo = [self repo];
    
    // when
    [self addANewCardWithABlueStroke:repo];
    
    // then
    KAMCard *card = [repo cardAtIndex:0];
    KAMColor *cardStrokeColor = card.strokeColor;
    XCTAssertEqual(cardStrokeColor.colorBlue, 1.0f, @"");
}

#pragma mark -

-(void)testDeletingACard
{
    // given
    KAMCardRepository *repo = [self repo];
    [self addANewCardWithABlueStroke:repo];
    
    // when
    // then
    [self removeFirstCard:repo];
}

-(void)testDeletingACardThenAddingANewOneDoesNotKeepAnyOfTheOldData
{
    // given
    KAMCardRepository *repo = [self repo];
    [self addANewCardWithABlueStroke:repo];
    [self addANewCardWithADefaultStroke:repo];
    [self removeFirstCard:repo];
    [self removeFirstCard:repo];
    
    // when
    [self addANewCardWithADefaultStroke:repo];
    
    // then
    KAMCard *card = [repo cardAtIndex:0];
    KAMColor *color = [card strokeColor];
    [self assertThatKAMColorIsBlack:color];
}

@end
