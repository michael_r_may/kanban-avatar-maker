MMImagePickerViewController
===========================

It's not big, and it's not clever, but it does make throwing a simple square image picker (camera or photo album) into your apps really quick and easy.
