A fast and simple way to generate the avatars you need for a Kanban, Scrum or other board where a physical representation is needed. 

It couldn't be simpler to use. Simply take/choose an image, add in the name and title, customise the colour scheme and hit print to send to your nearest air printer. Or share via email, message, air drop, or social networks.

Create cards for the whole team and print tiled across the page to save paper.